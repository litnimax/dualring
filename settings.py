LOG_LEVEL = 'INFO'

ARI_URL = 'http://192.168.56.101:8088'
ARI_USER = 'test'
ARI_PASSWORD = 'test'

RINGING_TIMEOUT = 2
ANSWER_TIMEOUT = 2
DIAL_TIMEOUT = 60
PREFIX_LEN = 4  # Four digits as prefix

PREFIXES = {
    '2561': ('2562', '2563'),
    '2571': ('2572', '2574'),
}