#!/usr/bin/env python2.7

import ari
import time
import logging
import re
import requests
import sys
from requests.exceptions import HTTPError, ConnectionError
import threading
from websocket._exceptions import WebSocketConnectionClosedException


LOG_LEVEL = 'INFO'

ARI_URL = 'http://192.168.56.101:8088'
ARI_USER = 'test'
ARI_PASSWORD = 'test'

RINGING_TIMEOUT = 10
ANSWER_TIMEOUT = 10
DIAL_TIMEOUT = 60
PREFIX_LEN = 4  # Four digits as prefix

PREFIXES = {
    '2561': ('2562', '2563'),
    '2571': ('2572', '2574'),
}

HUNT_GO_STATUSES = ["1", "2", "3", "17", "18", "19", "20", "21", "22", "23", "26", "27", "28",
                "29", "31", "34", "38", "41", "42", "47", "55", "57", "58", "65", "70", "79",
                "87", "88", "102", "111", "127", "400", "401", "402", "403", "404", "405",
                "406", "407", "408", "410", "413", "414", "415", "416", "420", "421", "423",
                "480", "481", "482", "483", "484", "485", "486", "487", "488", "500", "501",
                "502", "503", "504", "504", "513"]

# Import settings
try:
    from settings import *
except ImportError:
    pass


logging.basicConfig(level=eval('logging.%s' % LOG_LEVEL))

re_channel = re.compile('^(SIP/.+)-.+$')


def on_start(channel_obj, event):

    def ringing_timeout():
        logging.info('Ringing timeout, starting outgoing call 2.')
        init_outgoing2()


    def answer_timeout():
        logging.info('Answer timeout, starting outgoing call 2.')
        init_outgoing2()


    def state_change(channel, event):
        state = channel.json.get('state')
        if state == 'Ringing':
            logging.info('State Ringing: %s' % channel)

            if not incoming_ringing.is_set():  # Send ringing to the peer
                try:
                    incoming_ringing.set()
                    incoming.ring()
                except HTTPError as e:
                    if e.response.status_code == 404:
                        # NOT FOUND, channel was dropped.
                        logging.error('Incoming channel ring() failed!')
                        incoming_stasis_end(None,None)
                        return

            # Cancel ringing timer
            if outgoing_timer:
                outgoing_timer[0].cancel()
                del outgoing_timer[0]
            # Install answer timer
            outgoing_timer.append(threading.Timer(ANSWER_TIMEOUT, answer_timeout))
            outgoing_timer[0].start()


    def init_outgoing2():
        # Inidicate started condition
        outgoing2_started.set()
        # Originate the outgoing leg
        outgoing2 = client.channels.originate(endpoint='%s/%s' % (
                                                trunk_name,
                                                number_2),
                                             callerId=callerId,
                                             app='dualring',
                                             appArgs='outgoing2')
        outgoing2_channel.append(outgoing2)


        def answer_outgoing2(channel, event):
            # Terminate 1-st call
            if not outgoing_ended.is_set():
                try:
                    outgoing.hangup()
                except HTTPError as e:
                    if e.response.status_code == 404:
                        logging.warning('Could not find outgoing call to hangup.')
                        pass

            logging.info('Answered 2-nd call.')

            try:
                incoming.answer()
            except HTTPError as e:
                if e.response.status_code == 404:
                    logging.warning('Could not answer incoming from outgoing2!')
                    try:
                        outgoing2_ended.set()
                        outgoing2.hangup()
                    except HTTPError as e:
                        if e.response.status_code == 404:
                            logging.warning('Also could not hangup outgoing2!')
                    return

            try:
                outgoing2.answer()
            except HTTPError as e:
                if e.response.status_code == 404:
                    # Dead channel
                    outgoing2_ended.set()
                    return

            bridge.addChannel(channel=outgoing2.id)


        def state2_change(channel, event):
            state = channel.json.get('state')
            if state == 'Ringing' and not incoming_ringing.is_set():
                logging.info('State2 Ringing: %s' % channel)
                try:
                    incoming_ringing.set()
                    incoming.ring()
                except HTTPError as e:
                    if e.response.status_code == 404:
                        # NOT FOUND, but we don't care.
                        pass


        def outgoing2_destroyed(channel, event):
            cause = str(event['cause'])
            logging.info('Outgoing 2 is destroyed: %s, cause %s' % (channel, cause))
            outgoing2_ended.set()
            # If 1-st call is also ended then terminate
            if outgoing_ended.is_set() and not incoming_ended.is_set():
                try:
                    incoming.hangup()
                except HTTPError as e:
                    if e.response.status_code == 404:
                        # Incoming is already down there.
                        pass

        outgoing2.on_event('StasisStart', answer_outgoing2)
        outgoing2.on_event('ChannelStateChange', state2_change)
        outgoing2.on_event('ChannelDestroyed', outgoing2_destroyed)



    def incoming_stasis_end(channel, event):
        # This is called when incoming channel is destroyed by user side

        # Set the flag!
        incoming_ended.set()

        # Destroy bridge
        logging.info('Bridge destroy: %s' % bridge)
        bridge.destroy()

        if outgoing_started.is_set() and not outgoing_ended.is_set():
            logging.info('Hanging up outgoing channel.')
            try:
                outgoing.hangup()
            except HTTPError as e:
                if e.response.status_code == 404:
                    # Dead channel
                    logging.warning('Outgoing is already dead.')


        if outgoing2_started.is_set() and not outgoing2_ended.is_set():
            logging.info('Hanging up outgoing2 channel.')
            try:
                outgoing2_channel[0].hangup()
            except HTTPError as e:
                if e.response.status_code == 404:
                    # Dead channel
                    logging.warning('Outgoing2 is already dead.')

        # Clean up
        logging.info('Cleaning variables.')
        if outgoing_timer:
            outgoing_timer[0].cancel()
            del outgoing_timer[0]
        if outgoing2_channel:
            del outgoing2_channel[0]




    def outgoing_channel_destroyed(channel, event):
        cause = str(event['cause'])
        outgoing_ended.set()
        # This is called when outgoing channel is dexstroyed
        logging.info('outgoing_channel_destroyed: %s' % channel)

        # Cancel the timer immediately as we possible start 2-nd call now!
        if outgoing_timer:
            outgoing_timer[0].cancel()
            del outgoing_timer[0]

        # The 2-nd call was not yet started
        if not outgoing2_started.is_set():

            # Check if we can go on hunting
            if cause in HUNT_GO_STATUSES:
                logging.info('Hunting continues...')
                init_outgoing2()

            else:  # No hunting, hangup
                logging.info('No hunting for cause: %s' % cause)
                if not incoming_ended.is_set():
                    try:
                        incoming.hangup()
                    except HTTPError as e:
                        if e.response.status_code == 404:
                            # Incoming is already down there.
                            pass

        else:  # The 2nd call was started

            if outgoing2_started.is_set() and outgoing2_ended.is_set() and not incoming_ended.is_set():
                logging.info('The 2-nd call was ended, hangup incoming.')
                incoming.hangup()

            else:
                logging.info('The 2-nd call is in progress!')



    def answer_outgoing(channel, event):
        logging.info('Outgoing call answered.')

        # Cancel the next call timer
        if outgoing_timer:
            outgoing_timer[0].cancel()
            del outgoing_timer[0]

        # Terminate 2-nd call if it is progress
        if outgoing2_started.is_set() and not outgoing2_ended.is_set():
            try:
                outgoing2_ended.set()
                outgoing2_channel[0].hangup()
            except IndexError:
                # Rare case when outgoing is answered before outgoing2 originate completes
                def hangup_outgoing2_later():
                    if len(outgoing2_channel) == 1:
                        try:
                            outgoing2_channel[0].hangup()
                        except:
                            pass

                t = threading.Timer(2, hangup_outgoing2_later)
                t.start()

        # Answer outgoing and add it to the bridge
        outgoing.answer()
        bridge.addChannel(channel=outgoing.id)
        # Finally if all is ok answer incoming
        incoming.answer()



    # START IS HERE
    # Container used as global var
    outgoing2_channel = []
    outgoing_timer = []

    incoming = channel_obj.get('channel')

    if not event['args']:
        logging.error('Must have args!')
        return
    elif event['args'][0] in ['outgoing', 'outgoing2']:
        # Need to handle only one leg
        return
    elif event['args'][0] != 'incoming':
        return

    # Get the channel name
    channel_name = incoming.json.get('name')
    found_channel_name = re_channel.search(channel_name)
    if not found_channel_name:
        logging.error('Ahtung! Cannot match channel name!')
        incoming.hangup()
        return
    trunk_name = found_channel_name.group(1)

    # Get the callerId
    callerId = incoming.json['caller']['number']

    # Check prefixes
    exten = event['channel']['dialplan']['exten']
    prefix = exten[:PREFIX_LEN]
    number = exten[PREFIX_LEN:]
    if prefix not in PREFIXES.keys():
        logging.error('Unknown prefix, exit.')
        incoming.hangup()
        return
    prefix_1 = PREFIXES[prefix][0]
    prefix_2 = PREFIXES[prefix][1]
    number_1 = prefix_1 + number
    number_2 = prefix_2 + number


    # Create bridge
    bridge = client.bridges.create(type='mixing')
    bridge.addChannel(channel=incoming.id)

    # Init call trackers
    incoming_ended = threading.Event()
    incoming_ringing = threading.Event()
    outgoing_started = threading.Event()
    outgoing_ended = threading.Event()
    outgoing2_started = threading.Event()
    outgoing2_ended = threading.Event()

    # Originate the outgoing leg
    outgoing_started.set()
    outgoing = client.channels.originate(endpoint='%s/%s' % (trunk_name,
                                                             number_1),
                                         callerId=callerId,
                                         app='dualring',
                                         appArgs='outgoing',
                                         timeout=DIAL_TIMEOUT)

    incoming.on_event('StasisEnd', incoming_stasis_end)
    outgoing.on_event('ChannelDestroyed', outgoing_channel_destroyed)

    # Outgoing channel hooks
    outgoing.on_event('StasisStart', answer_outgoing)
    outgoing.on_event('ChannelStateChange', state_change)

    # Start outgoing timer
    outgoing_timer.append(threading.Timer(RINGING_TIMEOUT, ringing_timeout))
    outgoing_timer[0].start()


def on_start_with_pause(channel, event):
    t = threading.Timer(0.2, on_start, [channel, event])
    t.start()


try:
    client = ari.connect(ARI_URL, ARI_USER, ARI_PASSWORD)
    client.on_channel_event('StasisStart', on_start_with_pause)
    client.run(apps='dualring')

except WebSocketConnectionClosedException as e:
    logging.error('WebSocket connection is closed. Exit.')
    sys.exit(100)

except HTTPError as e:
    if e.response.status_code == 503:
        logging.error('Cannot connect to Asterisk WebSocket. Try again later.')
        sys.exit(101)

except ConnectionError:
    logging.error('Max retries exceeded connecting to Asterisk WebSocket. Try again later.')
    sys.exit(102)

